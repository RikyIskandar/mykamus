import React, { Component } from 'react';
import { Text, View, StatusBar, StyleSheet, TextInput, FlatList } from 'react-native'

let database = [
  { indonesia: 'Ayam', english: 'Chicken' },
  { indonesia: 'Anjing', english: 'Dog' },
  { indonesia: 'Kucing', english: 'Cat' },
  { indonesia: 'Kelinci', english: 'Rabbit' },
  { indonesia: 'Ikan', english: 'Fish' },
  { indonesia: 'Hiu', english: 'Shark' },
  { indonesia: 'Paus', english: 'Whale' },
  { indonesia: 'Lumba Lumba', english: 'Dolphins' },
]

export class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      data: database
    };
  }

  search = () => {
    let data = database;

    data = data.filter(item => item.indonesia.toLocaleLowerCase().includes(this.state.text.toLocaleLowerCase()));

    this.setState({
      data: data
    })
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <StatusBar backgroundColor="#ba2d65" barStyle="light-content" />

        <View style={styles.kotak}>
          <Text style={styles.teks1}>MyKamus</Text>
        </View>
        <TextInput
          style={{
            height: 40,
            borderColor: 'gray',
            borderWidth: 1,
            paddingLeft: 10,
            marginVertical: 20,
            marginHorizontal: 15,
            borderRadius: 20,
          }}
          onChangeText={text => this.setState({text: text})}
          value={this.state.text}
          placeholder="Masukkan Kata Disini"
          onKeyPress={() => this.search()}
        />

        <FlatList
        data={this.state.data}
        renderItem={({ item }) => 
        <View style={styles.flat}>
          <Text style={styles.teks2}>{item.indonesia}</Text>
          <Text style={styles.teks3}>{item.english}</Text>
        </View>
        }
        keyExtractor={item => item.indonesia}
        />

      </View>
    );
  }
}

export default App

const styles = StyleSheet.create({
  kotak:{
    padding: 20,
    backgroundColor: '#f06292',
    shadowColor: "#000",
    shadowOffset: {
	    width: 0,
	    height: 4,
    },  
    shadowOpacity: 0.32,
    shadowRadius: 5.46,
    elevation: 9,
  },
  teks1:{
    textAlign: 'center',
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 20,
  },
  teks2:{
    fontSize: 20,
    fontWeight: 'bold',
  },
  teks3:{
    fontSize: 18,
    marginTop: 3,
  },
  flat:{
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginVertical: 10, 
    marginHorizontal: 20,
  }
})
